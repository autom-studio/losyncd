# losyncd

循环 rsync 同步工具，启动守护进程，将配置文件中的文件列表循环同步到远程服务器。

## Build

```
git clone https://gitee.com/autom-studio/losyncd.git
cd losyncd
go build -o examples/losyncd
```

## Usage

### 前台启动

+ 修改配置文件： `examples/losyncd.yml`

+ 配置本机与远程 SSH 免密登录（**重要，否则会同步失败**）

+ 前台运行

```
./losyncd
```

### Systemd 托管（推荐）

+ 创建工作目录

```
# 默认是 /home/git/losyncd ，如果使用其他目录，请同步修改 examples/losyncd.service 中的 WorkingDirectory 和 ExecStart
mkdir /home/git/losyncd
```

+ 将二进制文件和配置文件复制到 workdir

```
cp examples/losyncd /home/git/losyncd/
cp examples/losyncd.yml /home/git/losyncd/
```

+ 将 systemd unit 文件复制到 `/lib/systemd//system/` 文件夹下

```
cp examples/losyncd.service /lib/systemd//system/
```

losyncd.service 中的 `User` 、 `Group` 字段默认为 `git` ，请根据实际同步用户修改

+ 启动 losyncd 服务

```
sudo systemctl daemon-reload
sudo systemctl enable --now losyncd
```
