package tool

import (
	"context"
	"fmt"

	_os "gitee.com/autom-studio/go-kits/os"
	"gitee.com/autom-studio/losyncd/internal/config"
	"go.uber.org/zap"
)

func rsyncPush(srcPath string, destPath string, host string, sshPort string) (err error) {
	commandLine := fmt.Sprintf("rsync -a --delete -e \"ssh -p %v -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null\" `readlink -f %s` %s:%s", sshPort, srcPath, host, destPath)
	_, err = _os.ExecCmd(commandLine)
	return
}

func rsyncPull(srcPath string, destPath string, host string, sshPort string) (err error) {
	commandLine := fmt.Sprintf("rsync -a --delete -e \"ssh -p %v -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null\" %s:%s %s", sshPort, host, srcPath, destPath)
	_, err = _os.ExecCmd(commandLine)
	return
}

func LoopSyncPush(ctx context.Context, resourceConfig config.ResourceConfigStruct) {
	for {
		select {
		case <-ctx.Done():
			//Exit infinite loop after receiving signal
			return
		default:
			for _, dir := range resourceConfig.DirList {
				srcPath := dir.SrcDir
				destPath := dir.DestDir
				for _, destHost := range resourceConfig.DestHosts {
					host := destHost.Host
					sshPort := destHost.Port
					err := rsyncPush(srcPath, destPath, host, sshPort)
					if err != nil {
						zap.L().Sugar().Errorf("rsync %s to %s failed: [%v]", srcPath, host, err)
					}
				}
			}
		}
	}
}

func LoopSyncPull(ctx context.Context, resourceConfig config.ResourceConfigStruct) {
	for {
		select {
		case <-ctx.Done():
			//Exit infinite loop after receiving signal
			return
		default:
			for _, dir := range resourceConfig.DirList {
				srcPath := dir.SrcDir
				destPath := dir.DestDir
				for _, destHost := range resourceConfig.DestHosts {
					host := destHost.Host
					sshPort := destHost.Port
					err := rsyncPull(srcPath, destPath, host, sshPort)
					if err != nil {
						zap.L().Sugar().Errorf("rsync %s to %s failed: [%v]", srcPath, host, err)
					}
				}
			}
		}
	}
}
