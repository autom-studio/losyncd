package execute

import (
	"context"

	"gitee.com/autom-studio/losyncd/internal/config"
	"gitee.com/autom-studio/losyncd/internal/tool"
)

func Execute(ctx context.Context, resourceConfig config.ResourceConfigStruct, mode string) {
	if mode == "push" {
		tool.LoopSyncPush(ctx, resourceConfig)
	} else if mode == "pull" {
		tool.LoopSyncPull(ctx, resourceConfig)
	}
}
