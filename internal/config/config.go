package config

type MainConfigStruct struct {
	Mode string `yaml:"mode"`
}

type LogConfigStruct struct {
	Path  string `yaml:"path"`
	Level string `yaml:"level"`
}

type DirListStruct struct {
	SrcDir  string `yaml:"srcDir"`
	DestDir string `yaml:"destDir"`
}

type DestHostStruct struct {
	Host string `yaml:"host"`
	Port string `yaml:"port"`
}

type ResourceConfigStruct struct {
	DirList   []DirListStruct  `yaml:"dirList"`
	DestHosts []DestHostStruct `yaml:"destHosts"`
}

type LosyncdConfig struct {
	Main     MainConfigStruct     `yaml:"main"`
	Log      LogConfigStruct      `yaml:"log"`
	Resource ResourceConfigStruct `yaml:"resource"`
}
